<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Authen_library
{
	private $ci;
	private $admin_session_id 	= "";
	private $admin_session 		= array();
	private $breadcrumb 		= array();
	private $admin_islogin 		= false;
	private $admin_issuper 		= NULL;

	public function __construct()
	{
		$this->ci 					=& get_instance();
		$this->ci->load->library('parser');
		$this->ci->load->library('Admin_library');
		$this->admin_session_id 	= ADMIN_SESS . md5($this->ci->input->server("HTTP_HOST"));
		$this->admin_session 		= $this->ci->session->userdata($this->admin_session_id);

		if($this->admin_session)
		{
			$this->admin_session 	= json_decode(base64_decode($this->admin_session),true);
			$this->admin_islogin 	= true;
		}
		else
		{
			// show_error('Session Expires', 404); exit;
			$this->admin_islogin 	= false;
			$this->forceLogin();
		}
	}
	public function forceLogin()
	{
		if($this->isLogin()==false)
		{
			admin_redirect("login?redirect=".$this->ci->uri->uri_string());
		}
	}
	public function isLogin()
	{
		return $this->admin_islogin;
	}

}